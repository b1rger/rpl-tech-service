<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Change password for LDAP/SMB/MAIL</title>
<link rel="stylesheet" type="text/css" href="style.css">
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/jquery.passroids.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('form').passroids({ 
		main: '#password',
		verify: '#password2',
		button: '#mysubmit',
		minimum: 2
	});
});
</script>
</head>
<body>
<?php
$message = array(); 
$debug = false;

if (isset($_POST['submit'])) {
	$username=$_POST['username'];
	$password=$_POST['password'];
	$pw1=$_POST['npw1'];
	$pw2=$_POST['npw2'];
	$pwdiffer=false; 
	$nogo=false;
	if (testinput($username) && testinput($password) && testinput($pw1, 1) && testinput($pw2)) {
		if (strcmp($pw1, $pw2)!=0) { 
			array_push($message, "The new passwords differ!"); 
		} else { 
			$mynewlmhash = lm_hash($pw2);
			$mynewnthash = nt_hash($pw2);
			$ldapconn = ldap_connect("ldaps://ldap.example.org/") or die("Error: Can't connect to LDAP Server!");
			ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
			$searchbase = "ou=people,dc=example,dc=org";
			if (ldap_bind($ldapconn)) {
				$sr = ldap_search($ldapconn, $searchbase, 'uid='.$username);
			}
			$entry = ldap_first_entry($ldapconn, $sr);
			$dn = ldap_get_dn($ldapconn, $entry);
			$name = ldap_get_values($ldapconn, $entry,"cn");
			$ldapbind = ldap_bind($ldapconn, $dn, $password);
			if($ldapbind) {
				$msg = "<p>Change password ";
				if(ldap_mod_replace ($ldapconn, $dn, array('userpassword' => "{CRYPT}".crypt($pw1)))) {
					if (!ldap_mod_add($ldapconn, $dn, array('sambaLMPassword' => $mynewlmhash))) {
						ldap_mod_replace($ldapconn, $dn, array('sambaLMPassword' => $mynewlmhash));
					}
					if (!ldap_mod_add($ldapconn, $dn, array('sambaNTPassword' => $mynewnthash))) {
						ldap_mod_replace($ldapconn, $dn, array('sambaNTPassword' => $mynewnthash));
					}
					if (!ldap_mod_add($ldapconn, $dn, array('sambaPwdLastSet' => date('U')))) {
						ldap_mod_replace($ldapconn, $dn, array('sambaPwdLastSet' => date('U')));
					}
					if (!ldap_mod_add($ldapconn, $dn, array('sambaPwdMustChange' => date('U')+307584000))) {
						ldap_mod_replace($ldapconn, $dn, array('sambaPwdMustChange' => date('U')+307584000));
					}
					$msg.="for ".$name[0]." succeeded"; 
				} else { 
					$msg.="failed"; 
				}
				array_push($message, $msg);
			} else {
				array_push($message, "Connection failed. Wrong password? Wrong username?");
			}
			ldap_close($ldapconn);
		}
	}
}


function testinput($string, $hard = 0) {
	global $message;
	if (empty($string)) {// || str_word_count($string)!=1) {
		array_push($message, "The username and all the passwords must be exact one word.");
		return false;
	} elseif ($hard==1 && checkhard($string) == -1) {
		array_push($message, "Passwords too short!");
		return false;
	} else {
		return true;
	}
}

function checkhard($string) {
	global $message;
	global $debug;
	$s = 0;
	if (strlen($string)<9) { $s=-1; }
	elseif (strlen($string)>7 && strlen($string)<16) { $s+=12; debug(">7>16: 16pkt");}
	elseif (strlen($string)>15) { $s+=18; debug(">15: 18pkt");}

	if (preg_match('/[a-z]/', $string)) { $s+=1; debug("a-z: 1pkt");}
	if (preg_match('/[A-Z]/', $string)) { $s+=5; debug("A-Z: 5pkt");}
	if (preg_match('/\d+/', $string)) { $s+=5; debug("d+: 5pkt");}
	if (preg_match('/(.*[0-9].*[0-9].*[0-9])/', $string)) { $s+=5; debug("mult.nr. 5pkt");}
	if (preg_match('/.[!,@,#,$,%,^,&,*,?,_,~]/', $string)) { $s+=5; debug("sz: 5pkt");}
	if (preg_match('/(.*[!,@,#,$,%,^,&,*,?,_,~].*[!,@,#,$,%,^,&,*,?,_,~])/', $string)) { $s+=5; debug("msz: 5pkt");}
	if (preg_match('/([a-z].*[A-Z])|([A-Z].*[a-z])/', $string)) { $s+=4; debug("g+k: 4pkt");}
	if (preg_match('/([a-zA-Z])/', $string) && preg_match('/([0-9])/', $string)) { $s+=4; debug("b+z: 4pkt");}
	if (preg_match('/([a-zA-Z0-9].*[!,@,#,$,%,^,&,*,?,_,~])|([!,@,#,$,%,^,&,*,?,_,~].*[a-zA-Z0-9])/', $string)) { $s+=7; debug("g,k,z: 7pkt");}
	//if (!((preg_match('/[a-z]/', $string) && preg_match('/[A-Z]/', $string)) || (preg_match('/[a-z]/', $string) && preg_match('/[0-9]/', $string)) || (preg_match('/[A-Z]/', $string) && preg_match('/[0-9]/', $string)))) { $s=-2; }
	if ($debug) {
		array_push($message, "Password got ".$s." Points");
	}
	return $s;
}

function debug($string) {
	global $debug;
	if ($debug) {
		print($string);
	}
}

function lm_hash($passwd) {
	$magic = pack('H16', '4B47532140232425');
  	$passwd = strtoupper($passwd);
  	if(strlen($passwd)>14) {
  	   $passwd=substr($passwd,0,14);
  	}
  	while (strlen($passwd)<14) {
		  $passwd.=chr(0);
  	}
  	$passwd = convert_key(substr($passwd,0,7)).convert_key(substr($passwd,7,7));
  	$lmstr="KGS!@#$%";
  	$td = mcrypt_module_open(MCRYPT_DES, '', MCRYPT_MODE_ECB, '');
  	mcrypt_generic_init($td, substr($passwd, 0, 8), $lmstr);
  	$enc1 = mcrypt_generic($td, $magic);
 	 $td = mcrypt_module_open(MCRYPT_DES, '', MCRYPT_MODE_ECB, '');
  	mcrypt_generic_init($td, substr($passwd, 8, 8), $lmstr);
  	$enc2 = mcrypt_generic($td, $magic);
  	return strtoupper(bin2hex($enc1.$enc2));
}



function convert_key($in_key) {
  $byte = array();
  $result = '';
  $byte[0] = substr($in_key, 0, 1);
  $byte[1] = chr(((ord(substr($in_key, 0, 1)) << 7) & 0xFF) |
    (ord(substr($in_key, 1, 1)) >> 1));
  $byte[2] = chr(((ord(substr($in_key, 1, 1)) << 6) & 0xFF) |
    (ord(substr($in_key, 2, 1)) >> 2));
  $byte[3] = chr(((ord(substr($in_key, 2, 1)) << 5) & 0xFF) |
    (ord(substr($in_key, 3, 1)) >> 3));
  $byte[4] = chr(((ord(substr($in_key, 3, 1)) << 4) & 0xFF) |
    (ord(substr($in_key, 4, 1)) >> 4));
  $byte[5] = chr(((ord(substr($in_key, 4, 1)) << 3) & 0xFF) |
    (ord(substr($in_key, 5, 1)) >> 5));
  $byte[6] = chr(((ord(substr($in_key, 5, 1)) << 2) & 0xFF) |
    (ord(substr($in_key, 6, 1)) >> 6));
  $byte[7] = chr((ord(substr($in_key, 6, 1)) << 1) & 0xFF);
  for ($i = 0; $i < 8; $i++) {
    $byte[$i] = set_odd_parity($byte[$i]);
    $result .= $byte[$i];
  }
  return $result;
}

function set_odd_parity($byte) {
  $parity = 0;
  $ordbyte = '';
  $ordbyte = ord($byte);
  for ($i = 0; $i < 8; $i++) {
    if ($ordbyte & 0x01) {$parity++;}
    $ordbyte >>= 1;
  }
  $ordbyte = ord($byte);
  if ($parity % 2 == 0) {
    if ($ordbyte & 0x01) {
      $ordbyte &= 0xFE;
    } else {
      $ordbyte |= 0x01;
    }
  }
  return chr($ordbyte);
}

function nt_hash($passwd) {
  return strtoupper(bin2hex(hash("md4", unicodify($passwd), TRUE)));
}

function unicodify($str) {
  $newstr = '';
  for ($i = 0; $i < strlen($str); ++$i) {
    $newstr .= substr($str, $i, 1) . chr(0);
  }
  return $newstr;
}
?>

<div id="main">
<h1>Change password for LDAP/MAIL/FILESERVER:</h1>
<h4>Depending on the strength of the password, the password gets points. <br />
The password must have at least 28 points.<br />
All passwords shorter than 8 characters have 0 points.<br /></h4>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
<table>
<tr>
<td>Username:</td>
<td><input type="text" name="username" size=20 maxlength=60 value="<?php echo $username; ?>" id="user_id"/></td>
</tr>
<tr>
<td>Password:</td>
<td><input type="password" name="password" size=20 maxlength=60"></td>
</tr>
<tr>
<td>New Password:</td>
<td><input type="password" name="npw1" size=20 maxlength=60" id="password"></td>
</tr>
<tr>
<td>Retype New Password:</td>
<td><input type="password" name="npw2" size=20 maxlength=60" id="password2"></td>
</table>
<input type="submit" value="Submit" name="submit" id="mysubmit"> 
</form>
<?php
echo "<div id='warning'><h2><center>";
foreach ($message as $msg) {
	echo $msg."<br />";
}
echo "</center></h2></div>";
?>
</div>
<hr>
<b><a href=http://derstandard.at/1281829236026/Passwoerter-immer-einfacher-zu-knacken">&rArr; Der Standard.at: Passwoerter immer einfacher zu knacken</a><br/></b>
<img src=dilbert_passwords.jpg>
</body>
</html>
